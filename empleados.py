class Empleado:
    # un ejemplo de clase para empleados
    def __init__(self, n, s):  # constructor
        self.nombre = n  # atributos
        self.nomina = s

    def CalculoImpuestos(self):  # metodo empleado
        return self.nomina * 0.30

    def __str__(self):
        return "El empleado {name} debe pagar {tax:.2f}".format(name=self.nombre, tax=self.CalculoImpuestos())


empleadoPepe = Empleado("Pepe", 20000)
empleadaAna = Empleado("Ana", 30000)

total = empleadoPepe.CalculoImpuestos() + empleadaAna.CalculoImpuestos()

print(empleadoPepe)
print(empleadaAna)

print("Los impuestos a pagar en total son {:.2f} euros".format(total))

